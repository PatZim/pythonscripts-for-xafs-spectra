# ---------------------------------------------------------------------------------------------------------------------------------------------------
# This is a file to illustrate a real world example. The important routines are in the file easyXAFS_Routines.py file which must be imported.
# In this file several examples are shown, most are rudimentary, but this should be sufficient to illustrate the use of each function.
# For most functions I used the names as they were also used in matheatica, this should make it easier for you to make the transition to python. 
# The easyXAFS_Routines.py file now also contains already some additional function that did not exist in mathematica.
# I hope these python examples are useful. 
#
# If you have any questions then just ask: Patric.Zimmermann@psi.ch or gmail
#
# ---------------------------------------------------------------------------------------------------------------------------------------------------
# This is a test script to illustrate a usecase for Cu spectra...
# Note: In this example also some CSV files are created from the data.
# This can be useful to import the Data into other programs for further analysis, e.g. Demeter/Athena or Larch.
# ---------------------------------------------------------------------------------------------------------------------------------------------------

import numpy as np
import easyXAFS_Routines as eXr



dataRootFolder = "RealCuData/"


thisDataI0 = "1069_I0_Cu_edge"              # Izero dataset, is used to normalise all the spectra

# here the subfolders/filenames are stored in a list. This makes it possible to process all folder in a loop.
# NOTE: Loop processing should be done only as "a first shot", usually each spectrum needs some special care.
# (e.g. adjustment of the range for the background fit.)
# To do this at the bottom of this file there are sections you can comment out as needed.
dataSets = ["1070_Cu_foil", "1074_CuO", "1075_Cu2O"]
numberOfScans = [1, 11, 11 ]              # this list containc the number of individual scans (e.g. here "CuFoil" was measured only once, the others 11 times)



# =================================================================================================================
# =================================================================================================================
# =================================================================================================================
# this method does all the processing for the selected dataset (is called at the bottom of this file.)
def makeAllPlots():

    print " ------------------------------------------------------------------------------------------------ "
    print " ------------------------------------------------------------------------------------------------ "

    fullPathI0 = dataRootFolder + thisDataI0 + "/" + thisDataI0 + "_1/" + thisDataI0 + "1_alldata_"
    fullPathData = dataRootFolder + thisDataSet + "/" + thisDataSet + "_1/" + thisDataSet + "1_alldata_"

    iZeroXY, resiZeroXY = eXr.combinedata(fullPathI0, 0, 29)                                                            # combine/average the 29 Izero scans

    iTransXY, resiTransXY = eXr.combinedata(fullPathData, 0, scanMax-1)
    eXr.saveXYdata2File(dataRootFolder + thisDataI0 + "_raw_AVG.csv", iZeroXY)            # saving an XY data set to a CSV file. The extension is optional, it will be added if not given
    eXr.saveXYdata2File(dataRootFolder + thisDataSet + "_Raw_iTransmission_AVG_1_alldata_0-" + str(scanMax-1) + ".csv", iTransXY)                          # Saving XY Data to a CSV file

    # # --- Plot RAW DATA ---------------------------------------------------------------------------------------
    # # Single plot of the RAW iZero and the iTransmission data
    # eXr.listLinePlot(iZeroXY,
    #                  titleStr="Raw Izero AVG: " + thisDataI0,
    #                  saveOn=dataRootFolder + thisDataI0 + "_Figure_Raw_AVG_Izero_" + "1_alldata_0-29.png")
    # eXr.listLinePlot(iTransXY,
    #                  titleStr="Raw iTransmission AVG: " + thisDataSet,
    #                  saveOn=dataRootFolder + thisDataSet + "_Figure_Raw_AVG_iTransmission_" + "1_alldata_0-" + str(scanMax-1) + ".png")
    #
    # eXr.listLinePlotDouble(iTransXY, iZeroXY,
    #                        titleStr1="Raw Transmission AVG" + thisDataSet,
    #                        titleStr2="Raw Izero AVG" + thisDataI0,
    #                        saveOn=dataRootFolder + thisDataSet +"_FigureDouble_XAFSmu_AND_iTransmission_AVG" + "1_alldata_0-" + str(scanMax-1) + ".png")

    eXr.listLinePlotDouble(iZeroXY, resiZeroXY,
                           titleStr1="Raw Izero AVG: " + thisDataI0,
                           titleStr2="Residuum",
                           saveOn=dataRootFolder + thisDataI0 + "_FigureDouble_Raw_AVG_Izero_" + "1_alldata_0-29.png")  # assign both titleStrings implicitly, saveFigure
    eXr.listLinePlotDouble(iTransXY, resiTransXY,
                           titleStr1="Raw iTransmission AVG: " + thisDataSet,
                           titleStr2="Residuum",
                           saveOn=dataRootFolder + thisDataSet + "_FigureDouble_Raw_AVG_iTransmission_" + "1_alldata_0-" + str(scanMax-1) + ".png")     # assign both titleStrings implicitly, saveFigure

    print " ------------------------------------------------------------------------------------------------ "
    ## Normaliseing the Transmission Data to iZero to see the disappearance of features introduced by the iZero
    iZeroNormSpec = eXr.iZeroNorm(iTransXY, iZeroXY)
    eXr.saveXYdata2File(dataRootFolder + thisDataSet + "_"  + thisDataI0 + "_AVG_Trans_IzeroNorm.csv", iZeroXY)         # saving an XY data set to a CSV file. The extension is optional, it will be added if not given

    eXr.listLinePlotDouble(iTransXY,iZeroNormSpec,
                           titleStr1="Raw Transmission Spectrum (AVG)" + thisDataSet,
                           titleStr2="iTrans (AVG) (" + thisDataSet + ") IzeroNorm (AVG) (" + thisDataI0 + ")",
                           saveOn=dataRootFolder + thisDataSet + "_FigureDouble_AVG_Trans-IzeroNorm.png")               # assign both titleStrings explicitly, saveFigure

    print " ------------------------------------------------------------------------------------------------ "
    # # Uncomment to show another BAD example of an Izero fit (linear fit of Izero does not work well if there are peaks in the iZero):
    # muXY, iZeroFit_XY = eXr.xafspoly(iTransXY, iZeroXY)
    # # print muXY                                                                                                        # DEBUG OUTPUT
    # eXr.listLinePlot(muXY, titleStr="XAFS_mu normalised to Polynomial Izero Fit (BAD FIT !!!)")
    # eXr.listLinePlot(iZeroFit_XY,titleStr="PolyFit of Izero (BAD -> removes peaks in Izero)")
    # eXr.listLinePlotFit(iZeroXY,iZeroFit_XY,titleStr="Polynomial Izero Fit (!!! BAD FIT !!!)")

    print " ------------------------------------------------------------------------------------------------ "
    print " ------------------------------------------------------------------------------------------------ "
    ## compute the XAFSmu from Lambert Beer:  mu = -Log(iTrans/iZero)
    mu_XY = eXr.xafspbpAuto(iTransXY,iZeroXY)                                                                           # computes mu from Lambert Beer // Note if any Yvalue < 0 it makes automatic Yshift
    eXr.listLinePlotDouble(mu_XY, iTransXY,
                           titleStr1="XAFS_mu = Log(transmission/I0) " + thisDataSet,
                           titleStr2="Raw Transmission Data  " + thisDataSet,
                           saveOn=dataRootFolder + thisDataSet + "_FigureDouble_XAFSmu_AND_iTransmission_AVG" + "1_alldata_0-" + str(scanMax-1) + ".png")

    eXr.saveXYdata2File(dataRootFolder + thisDataSet + "_XAFSmu_1_alldata_0-" + str(scanMax-1) + ".csv", mu_XY)                          # Saving XY Data to a CSV file


    print " ------------------------------------------------------------------------------------------------ "
    # # Uncomment to see another bad example, Computes XAFS mu from lin fitted background ==>> NOT GOOD! First compute XAFSmu and then align to linear background
    #
    # badFittedTransmission, linearFitXY = eXr.linbg(iTransXY, 8955, 8965)                        # Example of a Linear Background substraction (Linear for below main edge)
    # mu_XY_badLinFit = eXr.xafspbpAuto(badFittedTransmission,iZeroXY)               # computes mu fro Lambert Beer ( mu = -Log(iTrans/iZero)) // Note is Y < 0 it makes a shift
    #
    # eXr.listLinePlotDouble(mu_XY_badLinFit, badFittedTransmission,
    #                        titleStr1="XAFS_mu = Log(transmission/I0) with bad LinBackgrdFit " + thisDataSet,
    #                        titleStr2="Transmission Data with bad background " + thisDataSet,
    #                        saveOn=dataRootFolder + thisDataSet + "_FigureDouble_XAFSmu_From_iTrans_w_BadLinBackgrdFit_AVG" + "1_alldata_0-" + str(scanMax-1) + ".png")
    #

    print " ------------------------------------------------------------------------------------------------ "
    ## FIRST compute XAFS mu, and then substract linear background. (XAFS mu makes implicit iZero norm, introducing flatter preedged region !)

    fittedXAFSmu, linearFitXY = eXr.linbg(mu_XY, 8930, 8965)                        # Example of a Linear Background substraction (Linear for below main edge)
    eXr.saveXYdata2File(dataRootFolder + thisDataSet + "_XAFSmu_LinBackgrdFit_1_alldata_0-" + str(scanMax-1) + ".csv", fittedXAFSmu)                          # Saving XY Data to a CSV file

    # eXr.listLinePlotDouble(mu_XY, fittedXAFSmu,
    #                        titleStr1="XAFS_mu = Log(transmission/I0) w/o LinFit  " + thisDataSet,
    #                        titleStr2="XAFS_mu = Log(transmission/I0) then LinBackgrdFit " + thisDataSet,
    #                        saveOn=dataRootFolder + thisDataSet + "_FigureDouble_XAFSmuLinFit_AND_XAFSmu_wo_Fit_AVG" + "1_alldata_0-" + str(scanMax-1) + ".png")

    eXr.listLinePlotFit_Double(mu_XY, linearFitXY, eXr.yshift(fittedXAFSmu, +0.03),
                           titleStr1="XAFS_mu and LinBackgrdFit " + thisDataSet,
                           titleStr2="XAFS_mu minus LinBackgrdFit " + thisDataSet,
                           saveOn=dataRootFolder + thisDataSet + "_FigureDouble_XAFSmuLinFit_AND_XAFSmu_w_Fit_AVG" + "1_alldata_0-" + str(scanMax-1) + ".png")


    # eXr.showPlots()                   ## uncomment this to keep plots open, comment for loop processing



# # =================================================================================================================
# # =================================================================================================================
# # =================================================================================================================
# # DATA TREATMENT, plot and extract data into CSV files
# # =================================================================================================================
# # =================================================================================================================
# # =================================================================================================================
# # Uncomment/use this section to pick and treat one specific dataset (useful for adjustments or testing)
#
# setIndex = 2                            # select index of dataset (starts with 0)
# scanMax = numberOfScans[setIndex]       # number of scans per sample (see above)
# thisDataSet = dataSets[setIndex]        # select subfolder/filename (see above)
# makeAllPlots()                          # process selected dataset
#
# # =================================================================================================================
# # =================================================================================================================
# # =================================================================================================================
# # =================================================================================================================
# Loop selects each Datatset index, and run script for each set !!
# same as above, but processes all datasets
for i in range(0,3):
    setIndex = i
    scanMax = numberOfScans[setIndex]
    thisDataSet = dataSets[setIndex]

    makeAllPlots()

# eXr.showPlots()                       # uncommment this line to keep all plots open after the loop completes

# # =================================================================================================================
# # =================================================================================================================
# # =================================================================================================================
