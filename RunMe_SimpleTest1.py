# ---------------------------------------------------------------------------------------------------------------------------------------------------
# This is a file for testing. The important routines are in the file easyXAFS_Routines.py file which must be imported.
# In this file several examples are shown, most are rudimentary, but this should be sufficient to illustrate the use of each function.
# For most functions I used the names as they were also used in Matheatica, this should make it easier for you to make the transition to Python.
# The easyXAFS_Routines.py file now also contains already some additional function that did not exist in mathematica.
# I hope these python examples are useful. 
#
# If you have any questions then just ask: Patric.Zimmermann@psi.ch or gmail
#
# ---------------------------------------------------------------------------------------------------------------------------------------------------
# This is a test script to illustrate general usage of fileloading at simple plotting 
# ---------------------------------------------------------------------------------------------------------------------------------------------------

import numpy as np
from easyXAFS_Routines import *             # lazy way to import all functions in the file easyXAFS_Routines.py
# import easyXAFS_Routines as eXr           # this is sometimes a better way to import (matter fo personal preference)
                                            # NOTE: methods have then to be called as e.g. eXr.combinedatats(...)


dataFolder = "TestDataFiles/"


rawFileData = loadAllDataFileRaw(dataFolder + "1055_Cu_foil/1055_Cu_foil_1/1055_Cu_foil1_alldata_0")        # loads the complete dataset, including header
print np.matrix(rawFileData)

allData = loadAllDataFile(dataFolder + "1055_Cu_foil/1055_Cu_foil_1/1055_Cu_foil1_alldata_0")           # load the complete dataset, all columns without header
print np.matrix(allData)

xyData = grabdata(dataFolder + "1055_Cu_foil/1055_Cu_foil_1/1055_Cu_foil1_alldata_0")
print np.matrix(xyData)

xyData = grabcol(dataFolder + "1055_Cu_foil/1055_Cu_foil_1/1055_Cu_foil1_alldata_0", 1)
print np.matrix(xyData)

print " ------------------------------------------------------------------------------------------------ "

print " ---- X Shift... ----"
newXY = xshift(xyData, -8000)
print np.matrix(newXY)

print " ---- Y Shift... ----"
newXY = yshift(xyData, -70)
print np.matrix(newXY)

print "  ----Y Scale... ----"
newXY = rescale(xyData, 100)
print np.matrix(newXY)

print " ------------------------------------------------------------------------------------------------ "

# Further testing of the combinedatats() function, same as in mathematica: first argument is the filename, 2nd theta-shift, 3rd is the lowest 4th the highest number to be added to filename
#   all files covered by the index range are being averaged
# NOTE: I have changed the function to aso return the residuum of the averageing. (See easyXAFS_Routines.py, if you dont need/want that you can remove change the return-line) 


thisAVG_XY = combinedatats(dataFolder + "1055_Cu_foil/1055_Cu_foil_1/1055_Cu_foil1_alldata_", 0.0, 0, 1)[0]             # NOTE the [0] at the end to use only the 1st return value (only thisAVG_XY, the 2nd return value (residuum) is ignored); similar use [1] to fetch only the second  return value
thisAVG_XY, resiXY = combinedatats(dataFolder + "1055_Cu_foil/1055_Cu_foil_1/1055_Cu_foil1_alldata_", 0.0, 0, 1)
print thisAVG_XY
print combinedatats(dataFolder + "1055_Cu_foil/1055_Cu_foil_1/1055_Cu_foil1_alldata_", 0.0, 1, 1)
print combinedatats(dataFolder + "1055_Cu_foil/1055_Cu_foil_1/1055_Cu_foil1_alldata_", 0.0, 2, 2)
print combinedatats(dataFolder + "1055_Cu_foil/1055_Cu_foil_1/1055_Cu_foil1_alldata_", 0.0, 0, 1)

# combinedatats2() does the same as combinedatats(), ist is just a different implementation. (Maybe there are performance differences with large file. -- Feel free to test/compare both versions)

print combinedatats2(dataFolder + "1055_Cu_foil/1055_Cu_foil_1/1055_Cu_foil1_alldata_", 0.0, 0, 1)

listLinePlot(thisAVG_XY)                                                                  # listlineplot, no title, no saving of the figure


print " ------------------------------------------------------------------------------------------------ "


thisAVG_XY, resiXY = combinedata(dataFolder + "1055_Cu_foil/1055_Cu_foil_1/1055_Cu_foil1_alldata_", 0, 1)           # again, note that TWO XY data sets are returned, if you only assign one, it may thrown and error during further processing (if you save the result only in one variable, it will contain BOTH XY datasets!!!)
print thisAVG_XY
listLinePlot(thisAVG_XY,"ThisXY")

listLinePlot(rescale(thisAVG_XY, 0.001), "ThisXY is scaled by 0.0001")


showPlots()                                                                             # Required at the end of the script to keep plot windows open


