# ---------------------------------------------------------------------------------------------------------------------------------------------------
# This is a file for testing. The important routines are in the file easyXAFS_Routines.py file which must be imported.
# In this file several examples are shown, most are rudimentary, but this should be sufficient to illustrate the use of each function.
# For most functions I used the names as they were also used in matheatica, this should make it easier for you to make the transition to python. 
# The easyXAFS_Routines.py file now also contains already some additional function that did not exist in mathematica.
# I hope these python examples are useful. 
#
# If you have any questions then just ask: Patric.Zimmermann@psi.ch or gmail
#
# ---------------------------------------------------------------------------------------------------------------------------------------------------
# This is a test script showing the different plotting functions...
# NOTE: This example also saves some of the plots as PNG files, and it also creates some CSV files which can be imported into other programs
# ---------------------------------------------------------------------------------------------------------------------------------------------------

import numpy as np
import easyXAFS_Routines as eXr           # Note: Here I used the more explicit way to import the methods (see/compare to RunMe_SimpleTest1.py)


datafolder = "TestDataFiles/"

print " ------------------------------------------------------------------------------------------------ "

izeroXY = eXr.combinedata(datafolder + "1057_PZ_Cu_i0/1057_PZ_Cu_i0_1/1057_PZ_Cu_i01_alldata_", 0, 9)[0]              # NOTE the [0] at the end to use only the 1st return value (only izeroXY, the residuum is not used), similar use [1] for using only the second
# print izeroXY
eXr.listLinePlot(izeroXY)                                                                  # listlineplot, no title, no saving of the figure
eXr.listLinePlot(izeroXY, "1057_PZ_Cu_i01")                                                # listplot with titleString (first argument)
eXr.listLinePlot(izeroXY, titleStr="1057_PZ_Cu_i01")                                       # listplot with titleString (explicit assignment)
eXr.listLinePlot(izeroXY, saveOn=datafolder + "Figure_1057_PZ_Cu_i01_alldata_0-9.png")

print " ------------------------------------------------------------------------------------------------ "

izeroXY, residuumXY = eXr.combinedata(datafolder + "1057_PZ_Cu_i0/1057_PZ_Cu_i0_1/1057_PZ_Cu_i01_alldata_", 0, 9)   # not this is identical to using [0:1] at the end

eXr.listLinePlotDouble(izeroXY, residuumXY)
eXr.listLinePlotDouble(izeroXY, residuumXY, "Izero and Residdum")                                                           # assign the FIRST titleString
eXr.listLinePlotDouble(izeroXY, residuumXY, titleStr1 = "Izero and Residdum")                                               # assign the FIRST titleString explicitly
eXr.listLinePlotDouble(izeroXY, residuumXY, titleStr2 = "Izero and Residdum")                                               # assign the SECOND titleString explicitly
eXr.listLinePlotDouble(izeroXY, residuumXY, "Izero", "Residuum")                                                            # assign both titleStrings implicitly
eXr.listLinePlotDouble(izeroXY, residuumXY, "Izero", "Residuum", saveOn=datafolder + "FigureDouble_1057_PZ_Cu_i01_alldata_0-9.png")      # assign both titleStrings implicitly and save Figure to file


print " ------------------------------------------------------------------------------------------------ "
print " ------------------------------------------------------------------------------------------------ "


iZeroXY, resiZeroXY = eXr.combinedata(datafolder + "1057_PZ_Cu_i0/1057_PZ_Cu_i0_1/1057_PZ_Cu_i01_alldata_", 0, 9)           # import and combine (=average) the 0th to 9th spectrum
iTransXY, resiTransXY = eXr.combinedata(datafolder + "1058_PZ_CuCr2O4/1058_PZ_CuCr2O4_1/1058_PZ_CuCr2O41_alldata_", 0, 1)

eXr.saveXYdata2File(datafolder + "CuCr2O4_raw.csv", iTransXY)                           # saving an XY data set to a CSV file. The extension is optional, it will be added if not given


print " ------------------------------------------------------------------------------------------------ "


eXr.showPlots()                         # required to show all plots and keep them open.
