
# ---------------------------------------------------------------------------------------------------------------------------------------------------
# This is a final test script to just PLOT the results of the RunMe_Test4_RealCuData.py script which creates the CSV files used here!
# The spectra will be shown in one figure together to enable a comparison.
# A second figure is then also created which uses a Savitzky-Golay Filter to smoothen the data.
# For Infos on Savitzky-Golay see: https://en.wikipedia.org/wiki/Savitzky%E2%80%93Golay_filter
# The  Savitzky-Golay Filter is placed in a separate file filter.py as it is just copied from GitHub (see header in filter.py).
# To adjust the filter parameters see the method listLinePlotSameFig() in easyXAFS_Routines.py
# ---------------------------------------------------------------------------------------------------------------------------------------------------



import numpy as np
import easyXAFS_Routines as eXr



dataRootFolder = "RealCuData/"


thisDataI0 = "1069_I0_Cu_edge"              # Izero dataset, is used to normalise all the spectra

# here the subfolders/filenames are stored in a list. This makes it possible to process all folder in a loop.
# NOTE: Loop processing should be done only as "a first shot", usually each spectrum needs some special care.
# (e.g. adjustment of the range for the background fit.)
# To do this at the bottom of this file there are sections you can comment out as needed.
dataSets = ["1070_Cu_foil", "1074_CuO", "1075_Cu2O"]
numberOfScans = [1, 11, 11 ]              # this list containc the number of individual scans (e.g. here "CuFoil" was measured only once, the others 11 times)



# =================================================================================================================
# PLOT XAFS_mu into same figure to compare



offsets = [0.0, 0.0, 0.0]           # can be used to manually create an offset between the spectra
scales = [1, 1, 1 ]                 # can be used to manually scale each spectrum

legLabels = []                      # will be used to create the legend with the filenames

for j in range(0, 3):
    setIndex = j
    offset = offsets[j]             # NOTE: this uses the listed values above, will be overwritten when the offset is used below
    scale = scales[j]               # NOTE: this uses the listed values above, will be overwritten when the scale is used below

    scanMax = numberOfScans[setIndex]
    thisDataSet = dataSets[setIndex]


    ## assemble the list of labels based in the name
    legLabels.append(thisDataSet)
    legLabels.append(thisDataSet + " Filter")
    # print legLabels                                                                                                   # DEBUG OUTPUT


    ## load the Data from CSV file, returns csvData as numpy-array
    csvData = eXr.loadCSVDataFile(dataRootFolder + thisDataSet + "_XAFSmu_LinBackgrdFit_1_alldata_0-" + str(scanMax-1) , extension=".csv")

    # offset = sum(np.transpose(csvData)[1][0:20])  / 20.0          # average first 20 points to compute offset
    # scale = max(np.transpose(csvData)[1]-offset)                  # manually normalise to max,
    # # print offset                                                                                                      # DEBUG OUTPUT
    # # print scale                                                                                                       # DEBUG OUTPUT

    csvData = eXr.peaknorm(csvData)                                 # normalise spectrum, max = 1 (same as above)
    # csvData = eXr.yshift(csvData, +100)                             # shift Y data (intensity) by +100
    # csvData = eXr.xshift(csvData,-8000)                             # shift X data (energy) by -8000



    # plt, fig, ax = eXr.listLinePlotSameFig(999, np.transpose([np.transpose(csvData)[0] , (np.transpose(csvData)[1] + offsets[j])*scales[j]] ), titleStr="XAFS_mu = Log(transmission/I0)  " + thisDataSet, saveOn="TEST.png")
    plt, fig, ax = eXr.listLinePlotSameFig(998, np.transpose([np.transpose(csvData)[0] , (np.transpose(csvData)[1] - offset) / scale] ),
                                           legendData = legLabels[::2],              # use only every 2nd label (-> no filter data here), Slice notation is:  list[start_index:end_index:step]
                                           titleStr="XAFS_mu = Log(transmission/I0)",
                                           saveOn="AllFigsInOne.png")
    plt, fig, ax = eXr.listLinePlotSameFig(999, np.transpose([np.transpose(csvData)[0] , (np.transpose(csvData)[1] - offset) / scale] ),
                                           legendData = legLabels,
                                           titleStr="XAFS_mu = Log(transmission/I0) (Savitzky-Golay)",
                                           filter=True,
                                           saveOn="AllFigsInOne_w_Filter.png")





eXr.showPlots()

# # =================================================================================================================
# # =================================================================================================================
