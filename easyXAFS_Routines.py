# =============================================================================================================================================================
# ### This Python file "easyXAFS_Routines.py" was written by Patric Zimmermann, Dec 2018, Patric.Zimmermann (at) psi ch or Patric.Zimmermann (at) gmail com ###
# ### Feel free to use, alter or share it. However, I would appreciate if this header remains in this file also in subsequent versions.                     ###
# ### If you want to use this script for commercial purposes, then contact me first and get my permission.                                                  ###
# ### Mentioning my name in the Acknowledgement of any publication using this script would also be greately appreciated. Thank you.                         ### 
# =============================================================================================================================================================
# This file easyXAFS_Routines.py contains routines similar to the ones given by easyXAFs in the Matematica notebook, however, Python is freely available.
# For most functions I used the names as they were also used in Matematica, making it easier for you to make the transition to Python.
# The easyXAFS_Routines.py file also contains some additional functions that did not exist in Mathematica notebook (e.g. plotting residuals from averaging).
# Some lines are just prints or debug lines which you can comment out, and of course this file can be extended to your own liking. 
# After all this is the reason why I made this python script. - Not all is fully tested or fully implemented yet, future versions might add futher functions.
#
# If you have any questions then just ask: Patric.Zimmermann (at) psi ch or gmail --- I hope this is useful to some.
# =============================================================================================================================================================

import time
import numpy as np
import numpy.polynomial.polynomial as poly
from operator import truediv

import matplotlib as mpl
mpl.use("TkAgg")            # set the backend  (must be set before matplotlib.pyplot is imported)                            
# mpl.use("Qt4Agg")         # set the backend
import matplotlib.pyplot as plt

import filter as fil
# -----------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------

headerIndex = 2             # This is the index of the header line (is used to strip the head of the raw data file)

# ## Small
# figSizeSinglePlot = (7.6, 4.0)
# figSizeDoublePlot = (7.6, 6.15)
## Large
figSizeSinglePlot = (10.5, 5.5)
figSizeDoublePlot = (10.5, 8.5)



### -----------------------------------------------------------------------------------------------------------------------------
### -----------------------------------------------------------------------------------------------------------------------------
def defaultPlotStyle():
    # plt.gcf().set_size_inches(10.5, 7.5, forward=True)            # set/change figure size
    plt.xlabel("Energy in eV")                                      # set label for X axis
    plt.ylabel("Intensity")                                         # set label for Y axis
    plt.grid("on")

### -----------------------------------------------------------------------------------------------------------------------------
### --- This function must be called at the end of the script to keep the figure open. 
def showPlots():
    print " Plotting Done !!! (Close all Plots to exit script.)"
    plt.show()


### -----------------------------------------------------------------------------------------------------------------------------
### --- This function makes a ListLinePlot of an XY dataset.
def listLinePlotSameFig(figNum, xyData, legendData=None, titleStr=None, saveOn=False, filter=False, xPlotLimits = [None, None]):
    print " Plotting..."
    datX = np.transpose(xyData)[0]
    datY = np.transpose(xyData)[1]

    if filter:
        fig = plt.figure(figNum, figsize=figSizeSinglePlot)
        ax = plt.plot(datX, datY, '.', markersize=2)

        yhat = fil.savitzky_golay(datY, 9, 2)                       # filter/smooth data: window size 2, polynomial order 2
        lastColor = ax[0].get_color()                               # get color of previous plot to make filtered plot same color
        ax = plt.plot(datX, yhat, '-', color=lastColor)

        plt.xlim(xPlotLimits)  # set the xlimits for the filtered/smoothened plot

    else:
        fig = plt.figure(figNum, figsize=figSizeSinglePlot)
        ax = plt.plot(datX, datY, '-')

    defaultPlotStyle()  # Use the defaultPlotStyle() as defined above
    if not (titleStr == None):
        plt.title(titleStr)

    if legendData:
        plt.legend(legendData,loc=4, prop={'size': 10})

    plt.show(block=False)
    # save figure if saveOn is defined
    if saveOn:
        fig.savefig(saveOn, dpi=300)  # save the Figure with the name given via saveOn

    return plt, fig, ax


### -----------------------------------------------------------------------------------------------------------------------------
### --- This function makes a ListLinePlot of an XY dataset. 
def listLinePlot(xyData, titleStr = None, saveOn=False):
    print " Plotting..."
    datX = np.transpose(xyData)[0]
    datY = np.transpose(xyData)[1]
    fig = plt.figure(figsize=figSizeSinglePlot)
    plt.plot(datX, datY, 'b.-')
    defaultPlotStyle()                                      # Use the defaultPlotStyle() as defined above
    if not(titleStr == None):
        plt.title(titleStr)

    plt.tight_layout()                                      # tight layout (to avoid overlapping labels, fits axes to figure) --- See: https://matplotlib.org/users/tight_layout_guide.html
    plt.show(block=False)

    if saveOn:                                              # save figure if saveOn is True
        fig.savefig(saveOn, dpi=300)                        # save the Figure with the name given via saveOn
         
### -----------------------------------------------------------------------------------------------------------------------------
### --- This function makes a ListLinePlot of an XY dataset, and plots a line of the fit, 
### ---   the arguments titleStr1, titleStr2 and saveOn are optional (all should be a strings)  
def listLinePlotFit(xyData, xyfit, titleStr = None, saveOn=False):
    print " Plotting..."
    datX = np.transpose(xyData)[0]
    datY = np.transpose(xyData)[1]
    [fitX, fitY] = np.transpose(xyfit)
    fig = plt.figure(figsize=figSizeSinglePlot)
    plt.plot(datX, datY, 'b.-')                             # Plot the xyData  
    plt.plot(fitX, fitY, 'r-')                              # Plot the xyFit data
    defaultPlotStyle()                                      # Apply the defaultPlotStyle() as defined above
    if not(titleStr == None):                               # if the title string is not empty
        plt.title(titleStr)                                 #   then add the title to the plot
    plt.tight_layout()                                      # tight layout (to avoid overlapping labels, fits axes to figure) --- See: https://matplotlib.org/users/tight_layout_guide.html
    plt.show(block=False)                                   # show the figure with the plot and continue the script

    if saveOn:                                              # save figure if saveOn is True
        fig.savefig(saveOn, dpi=300)                        # save the Figure with the name given via saveOn



### -----------------------------------------------------------------------------------------------------------------------------
### --- This function makes a Double ListLinePlot for fits, it takes 3 XY datasets (xyData, xyfit, xyfitted)
### ---   the arguments titleStr1, titleStr2 and saveOn are optional
def listLinePlotFit_Double(xyData1, xyfit, xyfitted, titleStr1 = None, titleStr2 = None, saveOn=False):
    print " Plotting..."
    [data1_X, data1_Y] = np.transpose(xyData1)              # split first XY dataset into X and Y
    [fitX, fitY] = np.transpose(xyfit)
    [fitted_X, fitted_Y] = np.transpose(xyfitted)           # split second XY dataset into X and Y

    fig = plt.figure(figsize=figSizeDoublePlot)
    # --- Plot First dataset in top ---
    plt.subplot(211)                                        # make subplot with 1 column, 2 rows, put plot into top half
    plt.plot(data1_X, data1_Y, 'b.-')                       # Plot the xyData
    plt.plot(fitX, fitY, 'r-')                              # Plot the xyFit data
    defaultPlotStyle()                                      # Apply the defaultPlotStyle() as defined above
    if not (titleStr1 == None):                             # if the title string is not empty
        plt.title(titleStr1)                                # then add the title to the plot
    # --- Plot second dataset in bottom ---
    plt.subplot(212)                                        # make subplot with 1 column, 2 rows, put plot into bottom half
    plt.plot(fitted_X, fitted_Y, 'r-')                      # Plot the xyResi data
    defaultPlotStyle()                                      # Apply the defaultPlotStyle() as defined above
    if not (titleStr2 == None):                             # if the title string is not empty
        plt.title(titleStr2)                                # then add the title to the plot
    # -------------------------------------
    plt.tight_layout()                                      # tight layout (to avoid overlapping labels, fits axes to figure) --- See: https://matplotlib.org/users/tight_layout_guide.html
    plt.show(block=False)                                   # show the figure with the plot and continue the script
    # --- save figure if saveOn is True
    if saveOn:
        fig.savefig(saveOn, dpi=300)                        # save the Figure with the name given via saveOn


### -----------------------------------------------------------------------------------------------------------------------------
### --- This function makes a Double ListLinePlot of two XY datasets (data1_XY, data2_XY)
### ---   the arguments titleStr1, titleStr2 and saveOn are optional  
def listLinePlotDouble(data1_XY, data2_XY, titleStr1 = None, titleStr2 = None, saveOn=False):       
    print " Plotting..."
    [data1_X, data1_Y] = np.transpose(data1_XY)             # split first XY dataset into X and Y
    [data2_X, data2_Y] = np.transpose(data2_XY)             # split second XY dataset into X and Y

    fig = plt.figure(figsize=figSizeDoublePlot)
    # --- Plot First dataset in top ---
    plt.subplot(211)                                        # make subplot with 1 column, 2 rows, put plot into top half
    plt.plot(data1_X, data1_Y, 'b.-')                       # Plot the xyData
    defaultPlotStyle()                                      # Apply the defaultPlotStyle() as defined above
    if not(titleStr1 == None):                              #  if the title string is not empty
        plt.title(titleStr1)                                #   then add the title to the plot
    # --- Plot second dataset in bottom ---
    plt.subplot(212)                                        # make subplot with 1 column, 2 rows, put plot into bottom half
    plt.plot(data2_X, data2_Y, 'r-')                            # Plot the xyResi data
    defaultPlotStyle()                                      # Apply the defaultPlotStyle() as defined above
    if not(titleStr2 == None):                              #  if the title string is not empty
        plt.title(titleStr2)                                #   then add the title to the plot
    # -------------------------------------
    plt.tight_layout()                                      # tight layout (to avoid overlapping labels, fits axes to figure) --- See: https://matplotlib.org/users/tight_layout_guide.html
    plt.show(block=False)                                   # show the figure with the plot and continue the script
    # --- save figure if saveOn is defined ---
    if saveOn:
        fig.savefig(saveOn, dpi=300)                        # save the Figure with the name given via saveOn
  
### -----------------------------------------------------------------------------------------------------------------------------
### -----------------------------------------------------------------------------------------------------------------------------
### --- This function SAVES an XY dataset to a CSV file ---
def saveXYdata2File(fileName, xyData):
    if not( fileName[len(fileName)-4:len(fileName)].endswith('.csv') ):                 # See: https://stackoverflow.com/questions/14948900/strncmp-in-python
        fullFileName = fileName + ".csv"                                                # add .csv extension to fielName if it is not set
    else:
        fullFileName = fileName                                                         # if fileName end on .csv extension just use the fileName
        
    print "Saving XY dataset to file: ", fullFileName
    savefileCSV = open(fullFileName,'w')                                                # open the file for writing
    for n in range(0,len(xyData)):
        thisXY = xyData[n]                                                              # take first XY tupel as thisXY
        thisXY_line = str(thisXY[0]) + "," + str(thisXY[1]) + "\n"                      # create CSV data string from thisXY
        savefileCSV.write(thisXY_line);		                                            # here thisXY_line is saved to File

    savefileCSV.close()


### -----------------------------------------------------------------------------------------------------------------------------
### --- This function reads the alldata-text file ---
### --- returns the data content of the raw file as two-dimensional list (without header, uses the headerIndex defined above)
def loadCSVDataFile(fileName, extension=".csv"):                                        # default extension is .csv
    raw_data_all = loadAllDataFileRaw(fileName, extension)                              # gets the RAW DATA (complete text file with header)
    dataAll = []
    print "  loadCSVDataFile() - extracting data from file... "
    for i in range(headerIndex + 1, len(raw_data_all)):                                 # loop over raw data lines, skipping header (header has usuallt the index 2), creates a list of row-lists with the data
        # lineList = [float(n) for n in raw_data_all[i]]                                # convert list of value strings into a list of float values (explicitly via a loop)

        thisLine = raw_data_all[i][0]
        thisLineList = thisLine.split(",")
        # print thisLineList
        lineList = map(float, thisLineList)                                             # convert list of value strings into a list of float values (implicitly using map command)
        # print np.matrix(lineList)                                                     # DEBUG OUTPUT OF lineList
        dataAll.append(lineList)                                                        # append the row-list (each row: en, th, source, det, roi, tot, real, live, roilive, icr, ocr)

    # print dataAll                                                                     # DEBUG OUTPUT OF ALL DATA (only the data matrix without header)
    return np.array(dataAll)                                                            # returns the data from the file (all columns) as numpy array (needed f.e. to add xShift, yShift ect)


### -----------------------------------------------------------------------------------------------------------------------------
### --- This function reads the alldata-text file ---
### --- returns the complete data file with all columns 
def loadAllDataFileRaw(fileName, extension=".txt"):                                     # default extension is .txt
    raw_data_all = []                                                                   # initialise empty list
    fullFileName = fileName + extension                                                 # assemble the full filename
    print "Loading AllDataFileRaw: ", fullFileName
    with open(fullFileName,'r') as data_file:                                           # open the file for reading
        for thisLine in data_file:                                                      # iterate line by line over filecontent
            line = thisLine.strip("\r\n")                                               # remove all new lines
            line = line.split("\t")                                                     # split lines at \t (= tab) to make line into list
            raw_data_all.append(line)                                                   # appending line data as list
    
    #print raw_data_all                                                             # DEBUG OUTPUT OF ALL RAW DATA (complete text file with header)    
    return raw_data_all                                                                 # returns raw data from file (complete text file with header) 
    
### -----------------------------------------------------------------------------------------------------------------------------
### --- This function reads the alldata-text file ---
### --- returns the data content of the raw file as two-dimensional list (without header, uses the headerIndex defined above)
def loadAllDataFile(fileName):
    raw_data_all = loadAllDataFileRaw(fileName)                                         # gets the RAW DATA (complete text file with header)
    dataAll = []                                                                        
    print "  loadAllDataFile() - extracting data from file... "
    for i in range(headerIndex+1,len(raw_data_all)):                                    # loop over raw data lines, skipping header (header has usuallt the index 2), creates a list of row-lists with the data
        # lineList = [float(n) for n in raw_data_all[i]]                                # convert list of value strings into a list of float values (explicitly via a loop)
        lineList = map(float,raw_data_all[i])                                           # convert list of value strings into a list of float values (implicitly using map command)
        #print np.matrix(lineList)                                                  # DEBUG OUTPUT OF lineList 
        dataAll.append(lineList)                                                        # append the row-list (each row: en, th, source, det, roi, tot, real, live, roilive, icr, ocr)
    #print dataAll                                                                  # DEBUG OUTPUT OF ALL DATA (only the data matrix without header)
    return dataAll                                                                      # returns only the data from the file (all columns)
        
### -----------------------------------------------------------------------------------------------------------------------------
### --- This function fetches the alldata-text file and returns scaled ROI data as XY data (2 columns) ---
### --- returns an XY dataset = energy, roi
def grabdata(fileName):
    dataAll = loadAllDataFile(fileName)             
    print "  grabdata() - extracting XY data... Y = roi * icr/ocr "
    transDataAll = np.transpose(dataAll)                                                # transpose the list of rows into a list of columns
    # print transDataAll                                                            # DEBUG OUTPUT
    [en, th, source, det, roi, tot, real, live, roilive, icr, ocr] = transDataAll       # assign the list of columns to individual variables
    scale = icr / ocr                                                                   # calulate scale pointwise from icr/ocr columns
    scaledRoi = roilive*scale                                                           # scaling the roilive data pointwise with icr / ocr
    # print icr                                                                     # DEBUG OUTPUT OF icr
    # print ocr                                                                     # DEBUG OUTPUT OF ocr
    # print scale                                                                   # DEBUG OUTPUT OF scale = icr / ocr 
    dataXY = np.transpose([en, scaledRoi])
    return dataXY

### -----------------------------------------------------------------------------------------------------------------------------
### --- This function reads the alldata-text file and returns the data of the selected column ---
### returns X,Y = energy, column of Ydata selected by colIndex
### NOTE: List indices starts with 0 in Python (while in Mathematica it starts at 1)
def grabcol(fileName, colIndex):
    dataAll = loadAllDataFile(fileName)                 
    print "  grabcol() - extracting XY data... energy (0th column) and selected column  colIndex=", str(colIndex)
    transDataAll = np.transpose(dataAll)                                                # transpose the list of rows into a list of columns        
    [en, th, source, det, roi, tot, real, live, roilive, icr, ocr] = transDataAll       # assign the list of columns to individual variables
    if len(transDataAll) > colIndex:
        colData = transDataAll[colIndex]
    else:       
        print "WARNING: Column index out of range in grabcol()"
        colData = []
    dataXY = np.transpose([en,colData])                                                 # transpose the list of columns into a list of rows (is a list of X,Y tupels
    return dataXY

### -----------------------------------------------------------------------------------------------------------------------------
### --- This function returns the header line of the raw data file
### TODO Testing 
def grabheader(fileName):
    raw_data_all = loadAllDataFileRaw(fileName)
    #print raw_data_all                                                 # DEBUG OUTPUT OF ALL RAW DATA (complete text file with header)
    if headerIndex > len(raw_data_lines):                                   # check if index is valid (length check)
        headerList = raw_data_lines[headerIndex]                            # extract the list of headers from the raw data
    else:
        print "WARNING: No Column Headers found. Raw data not long enough."
        headerList = []
    return headerList
    

### -----------------------------------------------------------------------------------------------------------------------------
### --- adds and offset to the X values (usually energies) of an XY dataset 
def xshift(dat_XY, dx):
    # return np.transpose(np.transpose(dat_XY) + [dx,0])                    # add dx to all X values, add 0 to Y values
    return dat_XY + [dx,0]                                                  # add dx to all X values, add 0 to Y values

### -----------------------------------------------------------------------------------------------------------------------------
### --- adds and offset to the Y values of an XY dataset 
def yshift(dat_XY, dy):
    # return np.transpose(np.transpose(dat_XY) + [0,dy])                    # add 0 to X values, add dy to all Y values
    return dat_XY  + [0,dy]                                                 # add 0 to X values, add dy to all Y values

### -----------------------------------------------------------------------------------------------------------------------------
### --- scales the Y values of an XY dataset by the factor a
def rescale(dat_XY, a):
    # return np.transpose(np.transpose(dat_XY) * [1,a])                     # scale all X values by 1, scale all Y values by a
    return dat_XY * [1,a]                                                   # scale all X values by 1, scale all Y values by a

# -----------------------------------------------------------------------------------------------------------------------------
### --- normalises the Y values of an XY dataset to the maximum of the Y values
### optional argument normIntervall enables to choose a range for the peak normalisation (useful to select a peak)
def peaknorm(dat_XY, normInterval=[None,None]):
    [xVals, yVals] = np.transpose(dat_XY)
    yMax = np.max(yVals[normInterval[0]:normInterval[1]])                   # normIntervall by default it uses all data
    print "PeakNormValue: ", 1/yMax
    return np.transpose([xVals, yVals / yMax])                              # scale all Y values by 1/yMax 
    
### -----------------------------------------------------------------------------------------------------------------------------
### --- This function normalises the Y values of an XY dataset using the area (trapezoid integral) of a defined energy range (en_Min, en_Max)
def intnorm(dat_XY, en_Min, en_Max):
    [xVals, yVals] = np.transpose(dat_XY)                               
                                                                            
    # --- find XY values in energy range [en_Min, en_Max]                 
    intRangeX = []                                                          # initialise intRangeX
    intRangeY = []                                                          # initialise intRangeY
    for n in range(0,len(xVals)):                                       
        if (xVals[n] > en_Min) and (xVals[n] < en_Max):                     # if the xValue is within the selected energy range
            intRangeX.append(xVals[n])                                      # then append xValue to inRangeX
            intRangeY.append(yVals[n])                                      # then append yValue to inRangeY
    # print "intRangeX: ", intRangeX                                    # DEBUG OUPUT
    # print "intRangeY: ", intRangeY                                    # DEBUG OUPUT
    
    # --- Computing the area using a trapezoid (No interpolation)
    area = np.trapz(intRangeY,intRangeX)                                    # integrate the area in the defined energy range using a trapezoid. (See: https://docs.scipy.org/doc/scipy/reference/tutorial/integrate.html )
    if not(area == 0.0):
        print "IntNorm() - Integrating EnergyRange: [", intRangeX[0] , ",", intRangeX[len(intRangeX)-1],"] - Scaling Y values by integral =",str(area)         # DEBUG OUTPUT
        scaledDataXY = np.transpose([xVals, yVals / area])                  # scale all Y values by 1/area 
    else:
        print "NOTE: integral is zero !!! No Scaling will be applied due to div zero!!!"
        scaledDataXY = np.transpose([xVals, yVals])                         # scale all Y values by 1/area 
    # print scaledDataXY                                                # DEBUG OUPUT
    return scaledDataXY


def iZeroNorm(iTrans_XY,iZero_XY):
    [iTrans_X, iTrans_Y] = np.transpose(iTrans_XY)
    [iZero_X, iZero_Y] = np.transpose(iZero_XY)
    normSpecY = [y1/y2 for y1, y2 in zip(iTrans_Y, iZero_Y)]                                  # calculate the elementwise division iTrans_Y / iZero_Y

    return np.transpose([iTrans_X, normSpecY])


### -----------------------------------------------------------------------------------------------------------------------------
### --- This fuction averages the data from several files (from startIndex to endIndex)
def combinedata(fileNameBase, startIndex, endIndex):    
    numberOfSpectra = (endIndex+1 - startIndex)
    ### --- calculate the mean of all selected spectra
    for k in range(startIndex,endIndex+1):
        thisFileName = fileNameBase + str(k)
        xyData = grabdata(thisFileName)                                     # gets the XY data using the above defined grabdata() function
        # print k                                                       # DEBUG OUTPUT 
        # print xyData                                                  # DEBUG OUTPUT 
        if k == startIndex:
            sumData = xyData
        else:
            sumData = sumData + xyData
    print "  Averaging Data..."
    avgDataXY = sumData / numberOfSpectra                                     # diviging the summed matrix by the nummber of datasets
    ### ------------------------------------        
    ### --- calculate the residuum --------
    [avgDataX, avgDataY] = np.transpose(avgDataXY)
    for k in range(startIndex,endIndex+1):
        thisFileName = fileNameBase + str(k)
        xyData = grabdata(thisFileName)                                     # gets the XY data using the above defined grabdata() function
        [dataX, dataY] = np.transpose(xyData)
        if k == startIndex:
            residualSumY = dataY - avgDataY
        else:
            residualSumY = residualSumY + (dataY - avgDataY)                # Add the next residuum of yData, NOTE: renormalised by number of spectra below
    residuumY = residualSumY / numberOfSpectra                              # diviging the summed residuals by the nummber of datasets (renomrmalisation)
    # print residuumY                                                     # DEBUG OUTPUT
    resiXY = np.transpose([dataX, residuumY])
    ### ------------------------------------    
    # print len(sumData)                                                  # DEBUG OUTPUT
    # print len(resiXY)                                                   # DEBUG OUTPUT
    # print sumData[0]                                                    # DEBUG OUTPUT
    # print resiXY[0]                                                     # DEBUG OUTPUT

    return avgDataXY, resiXY

### Alternate version using the numpy.mean command (Should always give same result as combinedata (just without the residdum data)
### --- This fuction averages the data from several files (from startIndex to endIndex)
### TODO: compare performance with large files)
def combinedata2(fileNameBase, startIndex, endIndex):
    mergedData = []
    for k in range(startIndex,endIndex+1):
        thisFileName = fileNameBase + str(k)
        xyData = grabdata(thisFileName)                                     # gets the XY data using the above defined grabdata() function
        # print k                                                       # DEBUG OUTPUT
        # print xyData                                                  # DEBUG OUTPUT
        mergedData.append(xyData)                                   
    # print mergedData                                                  # DEBUG OUTPUT
    print "  Averaging Data..."
    avgData = np.mean(mergedData, axis=0)                                   # averaging using numpy mean command
    return avgData

### -----------------------------------------------------------------------------------------------------------------------------
### --- This fuction normalises a given raw XY spectrum to Izero
### --- NOTE: needs the raw XY mu data and the XY Izero data
### --- NOTE: both data sets must have same length and same energies)
def xafspbp(iTrans_XY, iZero_XY):
    [iTrans_X, iTrans_Y] = np.transpose(iTrans_XY)
    [iZero_X, iZero_Y] = np.transpose(iZero_XY)

    # --- check if the length of trans and iZero mathc
    if not (iTrans_X.all() == iZero_X.all()):
        print "WARNING: Energy mismatch  iT_X != i0_X  in xafspbp()",
    if not (iTrans_Y.all() == iZero_Y.all()):
        print "WARNING: Energy mismatch  iT_Y != i0_Y  in xafspbp()",

    # --- compute normalised XAFS_mu from intensities
    # listDiv = map(truediv, iTrans_Y, iZero_Y)                                         # calculate the elementwise division iTrans_Y / iZero_Y
    listDiv = [x / y for x, y in zip(iTrans_Y, iZero_Y)]  # calculate the elementwise division iTrans_Y / iZero_Y

    if all(x > 0 for x in listDiv):
        # mu_Y = -np.log(iTrans_Y / iZero_Y)                                        # calculate the mu from transission intensity (reverse Lambert Beer)
        mu_Y = -np.log(
            listDiv)  # calculate the mu from transission intensity (reverse Lambert Beer), Note listDiv must be positive for Log
        return np.transpose([iTrans_X, mu_Y])
    else:
        print " !!! CANNOT COMPUTE XAFS FOR NEGATIVE VALUES!!! (Log not defined!)"
        print listDiv

        shiftY = min(listDiv)
        print " ===>>> Shifting Y-data required!! Please apply Yshift of " + str(-shiftY)
        print "...returning bogous array: [[0,0], [1,0]]"
        time.sleep(10)

        return np.transpose([[0,0], [1,0]])


### -----------------------------------------------------------------------------------------------------------------------------
### --- This fuction normalises a given raw XY spectrum to Izero // NOTE make automatic Yshoft if Yvalue < 0 (Log not defined!)
### --- NOTE: needs the raw XY mu data and the XY Izero data
### --- NOTE: both data sets must have same length and same energies)
def xafspbpAuto(iTrans_XY, iZero_XY):
    [iTrans_X, iTrans_Y] = np.transpose(iTrans_XY)
    [iZero_X, iZero_Y] = np.transpose(iZero_XY)

    # --- check if the length of trans and iZero mathc
    if not (iTrans_X.all() == iZero_X.all()):
        print "WARNING: Energy mismatch  iT_X != i0_X  in xafspbp()",
    if not (iTrans_Y.all() == iZero_Y.all()):
        print "WARNING: Energy mismatch  iT_Y != i0_Y  in xafspbp()",

    # --- compute normalised XAFS_mu from intensities
    # listDiv = map(truediv, iTrans_Y, iZero_Y)                                         # calculate the elementwise division iTrans_Y / iZero_Y
    listDiv = [x / y for x, y in zip(iTrans_Y, iZero_Y)]  # calculate the elementwise division iTrans_Y / iZero_Y

    if all(x > 0 for x in listDiv):
        # mu_Y = -np.log(iTrans_Y / iZero_Y)                                        # calculate the mu from transission intensity (reverse Lambert Beer)
        mu_Y = -np.log(
            listDiv)  # calculate the mu from transission intensity (reverse Lambert Beer), Note listDiv must be positive for Log
        return np.transpose([iTrans_X, mu_Y])
    else:
        print listDiv
        print " !!! CANNOT COMPUTE XAFS FOR NEGATIVE VALUES!!! (Log not defined!)"
        print listDiv

        shiftY = min(listDiv)
        print shiftY
        shifted_listDiv = listDiv - (
                    shiftY * 1.0001)  # using 1.0001 (0.01% more) for shift, to avoid having exact zero as minimum
        print shifted_listDiv
        print " ===>>> shifting Y-data... by: " + str(-shiftY)
        mu_Y_shifted = -np.log(shifted_listDiv)  # calculate the mu from shifted transission intensity (Lambert Beer)
        time.sleep(2)

        return np.transpose([iTrans_X, mu_Y_shifted])


# -----------------------------------------------------------------------------------------------------------------------------
### --- This fuction normalises a given raw XY spectrum to polynom fit of Izero
### --- !!! BAD FIT for Izero !!!: this does not work well for Izero, because it will not reproduce peaks well 
def xafspoly(iTrans_XY, iZero_XY):
    polyOrderFit = 5                                                                    # polynom order of the fit (1: linear, 2:square, 3:cube,...) e.g. {1, x, x^2, x^3, x^4, x^5} --- (Feel free to play with the order of the polynomial)
    print "!!! BAD FIT WARNING !!! Some tests show that xafspoly() fits Izero poorly. - I hope you know what you are doing..."
    [iTrans_X, iTrans_Y] = np.transpose(iTrans_XY)
    [iZero_X, iZero_Y] = np.transpose(iZero_XY)
    
    ### --- Fit the iZero with a polynom of the order (polyOrderFit)
    coeff = poly.polyfit(iZero_X, iZero_Y, polyOrderFit)                            # coeff holds the (polyOrder+1) coefficients of the polynom fit (lowest to highest: e.g. ax^0 + bx^1 + cx^2 for polyOrder=2)
    # print "xafspoly() - Fit Coefficients: ", coeff                                # DEBUG OUPUT
    iZeroFit_Y = poly.polyval(iZero_X, coeff)
    # print fittedY                                                                 # DEBUG OUPUT
    mu_Y = -np.log(iTrans_Y / iZeroFit_Y)                                           # calculate the mu from transission intensity (reverse Lambert Beer)
    
    mu_XY = np.transpose([iTrans_X, mu_Y])                                          # put the X and Y values of mu together to create an XY-dataset
    iZeroFit_XY = np.transpose([iZero_X, iZeroFit_Y])                               # put the X and Y values of the fit together to create an XY-dataset
    return mu_XY, iZeroFit_XY                                                       # returns the muXY data and the Izero fit
    # return mu_XY                                                                  # uncomment to return only the muXY data (comment line above)


### -----------------------------------------------------------------------------------------------------------------------------
### --- This fuction adds as calibrating shift to the theta angle, which is then directly transleted onto the energy axis (X values)
### TODO Further Testing (1st test looks ok)
def thshift(fileName, shiftTheta):
    dataAll = loadAllDataFile(fileName)             
    transDataAll = np.transpose(dataAll)                                                # transpose the list of rows into a list of columns
    # print transDataAll
    [en, th, source, det, roi, tot, real, live, roilive, icr, ocr] = transDataAll       # assign the list of columns to individual variables
    scale = icr / ocr                                                                   # calulate scale pointwise from icr/ocr columns
    E0 = en * np.sin(th * np.pi/180 )                                                   # Get energy E0 from theta --- NOTE: np.pi/180 to convert Radians into degree
    en2 = E0 / np.sin( (th + shiftTheta) * np.pi/180 )                                  # Get new energy en2 from shifted theta --- NOTE: np.pi/180 to convert Radians into degree
    # print scale                                                                   # DEBUG OUTPUT OF scale = icr / ocr 
    # print E0                                                                      # DEBUG OUTPUT OF icr
    # print en2                                                                     # DEBUG OUTPUT OF ocr   
    scaledRoi = roilive*scale                                                           # scaling the roilive data pointwise with icr / ocr
    dataXY = np.transpose([en2, scaledRoi])
    return dataXY

### -----------------------------------------------------------------------------------------------------------------------------
### --- This fuction averages the data from several files (from startIndex to endIndex) and also adds a theta-shift using the thshift() function
### ---   returns to XY datatsets, the averaged data, and the residuum
def combinedatats(fileNameBase, shift, startIndex, endIndex):  
    print "CombineDataTS() - ThetaShift: ", str(shift), " IndexRange: [", str(startIndex), ",", str(endIndex),"]"
    numberOfSpectra = (endIndex+1 - startIndex)                                             # calcuate the number fo spectra = maxIndex-minIndex (used to renormalise)
    ### --- calculate the mean of the selected spectra
    for k in range(startIndex,endIndex+1):
        thisFileName = fileNameBase + str(k)
        xyData = thshift(thisFileName, shift)
        # print k                                                                           # DEBUG OUTPUT 
        # print xyData                                                                      # DEBUG OUTPUT 
        if k == startIndex:
            sumData = xyData
        else:
            if not(len(sumData) == len(xyData)):                                                # CHECKING length of datatsets, will trow an error if length is not matching
                print "!!! ERROR in combinedatats() !!! DataLength mismatch: ", len(sumData), " != ", len(xyData)
            sumData = sumData + xyData                                                          # Add the next XY dataset, NOTE: divided by number of spectra below
        # print sumData                                                                     # DEBUG OUTPUT     
    avgData = sumData / numberOfSpectra                                                         # diviging the summed matrix by the nummber of datasets (renomrmalisation)
    ### ---------------------------------------------------        
    ### --- calculated the residuum of the selected spectra
    for k in range(startIndex,endIndex+1):
        thisFileName = fileNameBase + str(k)
        xyData = thshift(thisFileName, shift)
        if k == startIndex:
            residualSum = xyData - avgData
        else:
            residualSum = residualSum + (xyData - avgData)                                      # Add the next XY residuum, NOTE: renormalised by number of spectra below
        # print residuum                                                                     # DEBUG OUTPUT     
    residuum = residualSum / numberOfSpectra                                                    # diviging the summed matrix by the nummber of datasets (renomrmalisation)
    ### ---------------------------------------------------        
    return avgData, residuum                                                                    # returns the AVG data and the residuum of the averaging
    # return avgData                                                                            # uncomment to return only the AVG data (comment line above)                                              


### Alternate version using the numpy.mean command (Should always give same result as combinedatats --- TODO: compare performance with large files)
### --- This fuction averages the data from several files (from startIndex to endIndex) and also adds a theta-shift using thshift()
### --- Returns two XY datasets: averaged XY data and residual XY data
def combinedatats2(fileNameBase, shift, startIndex, endIndex):
    ### --- calculate the mean of all selected spectra
    mergedData = []
    for k in range(startIndex,endIndex+1):
        thisFileName = fileNameBase + str(k)
        xyData = thshift(thisFileName, shift)
        # print k                                                                   # DEBUG OUTPUT
        # print xyData                                                              # DEBUG OUTPUT
        mergedData.append(xyData)                                                       # Append the next XY dataset, created a list with all selected datasets
        # print mergedData                                                          # DEBUG OUTPUT
    avgData = np.mean(mergedData, axis=0)                                               # vertical Mean, averaging columns --- Does not work with dimension > 2 (??)
    # avgData = np.mean(mergedData, axis=1)                                             # horizontal Mean, averaging rows --- Does not work with dimension > 2 (??)
    ### ------------------------------------        
    ### --- calculated the residuum --------
    residualData = []
    for k in range(startIndex,endIndex+1):
        thisFileName = fileNameBase + str(k)
        xyData = thshift(thisFileName, shift)
        # print k                                                                   # DEBUG OUTPUT
        # print xyData                                                              # DEBUG OUTPUT
        residualData.append(xyData-avgData)                                             # Append the next residuum, created a list with all residuals
        # print mergedData                                                          # DEBUG OUTPUT
    residuum = np.mean(residualData, axis=0)                                            # Mean of all individual residual
    ### ------------------------------------    
    return avgData, residuum                                                            # returns the AVG data and the residuum of the averaging


### -----------------------------------------------------------------------------------------------------------------------------
### --- This fuction substrated the background using a polynomial fit on the given energy range
### The value of polyOrder defines the order of the polynom, here polyOrder=1 makes a linear fit --- TODO: Test maybe also a square polynomial fit
### --- Returns two XY datasets: bachground substracted XY data and background XY data
def linbg(dat_XY, en_Min, en_Max):
    polyOrder = 1                                                                       # polynom order of the fit (1: linear, 2:square, 3:cube,...) 
    [xVals, yVals] = np.transpose(dat_XY)                                           
                                                                                        
    ### find XY values in energy range [en_Min, en_Max]                             
    fitRangeX = []                                                                      # initialise intRangeX
    fitRangeY = []                                                                      # initialise intRangeY
    print " Finding energy values to be used for Linear Background Fit..."
    for n in range(0,len(xVals)):
        print en_Min, " < ", xVals[n], " < ", en_Max, " --> ", en_Min < xVals[n] < en_Max
        if (xVals[n] > en_Min) and (xVals[n] < en_Max):                                 # if the xValue is within the selected energy range
            fitRangeX.append(xVals[n])                                                  # then append xValue to inRangeX
            fitRangeY.append(yVals[n])                                                  # then append yValue to inRangeY
    # print "intRangeX: ", intRangeX                                                # DEBUG OUPUT
    # print "intRangeY: ", intRangeY                                                # DEBUG OUPUT

    ### Fit the selected energy range with a polynom of the order (polyOrder)
    coeff = poly.polyfit(fitRangeX, fitRangeY, polyOrder)                               # coeff holds the (polyOrder+1) coefficients of the polynom fit (lowest to highest: e.g. ax^0 + bx^1 + cx^2 for polyOrder=2)
    # print "LinBG2() - Fit Coefficients: ", coeff                                  # DEBUG OUPUT

    backgroundFit_Y = poly.polyval(xVals, coeff)                                        # apply fit coefficients to polynom and compute Y values
    # print backgroundFit_Y                                                         # DEBUG OUPUT

    newXY = np.transpose([xVals, yVals - backgroundFit_Y])                              # substract linear fit from Y values
    backgroundFit_XY = np.transpose([xVals, backgroundFit_Y])                           # substract linear fit from Y values
    return newXY, backgroundFit_XY
    
# -----------------------------------------------------------------------------------------------------------------------------





