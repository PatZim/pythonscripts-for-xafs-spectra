# PythonScripts for XAFS Spectra

Here I share a few Python files which I wrote for the treatment of XAFS spectra.

The main intention to write this was, to enable owners and users of the easyXAFS 
spectrometer to treat their data without the need for Mathematica.
To the best of my knowledge the company does to date, Jan 2019, only provide a 
simple Mathematica Notebook for data treatment. But since this requires a license 
and many preople prefer Python, I translated it into Python, and extended it with 
additional functionality (e.g. residuals for averaging).

I hope these script are useful for some. 


See Wiki for Screenshots: https://gitlab.com/PatZim/pythonscripts-for-xafs-spectra/wikis/home