# ---------------------------------------------------------------------------------------------------------------------------------------------------
# This is a file for testing. The important routines are in the file easyXAFS_Routines.py file which must be imported.
# In this file several examples are shown, most are rudimentary, but this should be sufficient to illustrate the use of each function.
# For most functions I used the names as they were also used in matheatica, this should make it easier for you to make the transition to python. 
# The easyXAFS_Routines.py file now also contains already some additional function that did not exist in mathematica.
# I hope these python examples are useful. 
#
# If you have any questions then just ask: Patric.Zimmermann@psi.ch or gmail
#
# ---------------------------------------------------------------------------------------------------------------------------------------------------
# This is a test script showing the different plotting functions...
# Note: In this example also some CSV files are created from the data, this can be useful to import the Data into other programs.
# ---------------------------------------------------------------------------------------------------------------------------------------------------

import numpy as np
import easyXAFS_Routines as eXr



dataFolder = "TestDataFiles/"

print " ------------------------------------------------------------------------------------------------ "
print " ------------------------------------------------------------------------------------------------ "


iZeroXY, resiZeroXY = eXr.combinedata(dataFolder + "1057_PZ_Cu_i0/1057_PZ_Cu_i0_1/1057_PZ_Cu_i01_alldata_", 0, 9)   # not this is identical to using [0:1] at the end
iTransXY, resiTransXY = eXr.combinedata(dataFolder + "1058_PZ_CuCr2O4/1058_PZ_CuCr2O4_1/1058_PZ_CuCr2O41_alldata_", 0, 1)

eXr.saveXYdata2File(dataFolder + "CuCr2O4_raw.csv", iTransXY)            # saving an XY data set to a CSV file. The extension is optional, it will be added if not given


print " ------------------------------------------------------------------------------------------------ "


# eXr.listLinePlot(iZeroXY)
eXr.listLinePlot(iZeroXY,"Raw Izero")
eXr.listLinePlot(iTransXY,"Raw iTransmission")

muXY, iZeroFit_XY = eXr.xafspoly(iTransXY, iZeroXY)
# print muXY
eXr.listLinePlot(muXY,"XAFS_mu normalised to Polynomial Izero Fit")
eXr.listLinePlot(iZeroFit_XY,"Polynomial Fit of Izero")

eXr.listLinePlotFit(iZeroXY,iZeroFit_XY,"Polynomial Izero Fit (!!! BAD FIT !!!)")

fittedXY, linearFitXY = eXr.linbg(iTransXY, 8930, 8980)                                # Example of a Linear Background substraction (Linear fit below main edge)

eXr.saveXYdata2File(dataFolder + "CuCr2O4_LinFitted.csv", fittedXY)                     # Saving XY Data to a CSV file

print " ------------------------------------------------------------------------------------------------ "
print " ------------------------------------------------------------------------------------------------ "


eXr.listLinePlot(iTransXY,"Raw Transmission Data")
eXr.listLinePlotFit(iTransXY,linearFitXY,"Raw Transmission Data & Linear Background Fit")
eXr.listLinePlot(fittedXY, "Transmission with substracted Linear Fitted Background ")
#
# print len(fittedXY)
# print len(iZeroXY)
#
mu_XY = eXr.xafspbp(iTransXY,iZeroXY)
# eXr.listLinePlot(mu_XY, "XAFS_mu = Log(transmission/I0)")
eXr.listLinePlotDouble(mu_XY, iTransXY, "XAFS_mu = Log(transmission/I0)", "Raw Transmission Data")

mu_XY = eXr.xafspbp(eXr.yshift(fittedXY,+5000),iZeroXY)                                   # NOTE: here a Yshift is needed, because Log must not have negative input (-> xafspbpAuto() adds automatically the required offset )
eXr.listLinePlot(mu_XY, "XAFS_mu with substracted Linear Fitted Background ")
eXr.listLinePlotDouble(mu_XY, eXr.yshift(fittedXY,+5000), "XAFS_mu with substracted Linear Fitted Background", "Transmission with substracted Linear Fitted Background (Shift: Y+5000)")

print " ------------------------------------------------------------------------------------------------ "


iZeroXY, resiZeroXY = eXr.combinedata(dataFolder + "1057_PZ_Cu_i0/1057_PZ_Cu_i0_1/1057_PZ_Cu_i01_alldata_", 0, 9)
iTransXY, resiTrans = eXr.combinedata(dataFolder + "1058_PZ_CuCr2O4/1058_PZ_CuCr2O4_1/1058_PZ_CuCr2O41_alldata_", 0, 1)
# print thisXY
eXr.listLinePlot(iTransXY)

imuXY = eXr.xafspbp(iTransXY,iZeroXY)
# print imuXY
eXr.listLinePlot(imuXY)


imuXY, fitXY = eXr.xafspoly(iTransXY,iZeroXY)
# print imuXY
eXr.listLinePlot(imuXY,"Background PolyFit (!!! BAD FIT !!!)")


eXr.listLinePlotFit(iZeroXY,fitXY, "PolyFit")

print " ------------------------------------------------------------------------------------------------ "


eXr.showPlots()




